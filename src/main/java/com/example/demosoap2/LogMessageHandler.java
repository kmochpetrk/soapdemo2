package com.example.demosoap2;


import com.example.demosoap2.services.MessageService;
import lombok.extern.slf4j.Slf4j;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

@Slf4j
public class LogMessageHandler implements SOAPHandler<SOAPMessageContext> {


    private MessageService messageService;

    @Override
    public Set<QName> getHeaders() {
        return Collections.EMPTY_SET;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        SOAPMessage msg = context.getMessage(); //Line 1
        //log.info(((HeaderList)context.get(JAXWSProperties.INBOUND_HEADER_LIST_PROPERTY)).get(0).getStringContent());
        //((HeaderList)((SOAPMessageContextImpl) context).getMessageContext().get(JAXWSProperties.INBOUND_HEADER_LIST_PROPERTY)).get(0).getStringContent()
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            msg.writeTo(baos);  //Line 3
            final String baosStr = baos.toString();
            log.info(baosStr);
            messageService.saveMessageReq(baosStr);
        } catch (Exception ex) {
            Logger.getLogger(LogMessageHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return true;
    }

    @Override
    public void close(MessageContext context) {
    }

    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }
}
