package com.example.demosoap2;

import com.examplesoap.demosoap2.accounts.generated.BottomUpServiceInterface;
import com.examplesoap.demosoap2.accounts.generated.GlAccount;
import org.springframework.stereotype.Component;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@Component
@WebService(endpointInterface = "com.examplesoap.demosoap2.accounts.generated.BottomUpServiceInterface")
public class BottomUpServiceImpl implements BottomUpServiceInterface {
    @Override
    public GlAccount getAccountByAcctno(GlAccount arg0) {
        GlAccount glAccount = new GlAccount();
        glAccount.setAcctNo(arg0.getAcctNo() + "100");
        return glAccount;
    }

    @Override
    public GlAccount createNew(GlAccount arg0) {
        return arg0;
    }

    @Override
    public List<GlAccount> getAllAccounts() {
        List<GlAccount> retList = new ArrayList<>();
        GlAccount glAccount = new GlAccount();
        glAccount.setAcctNo("211");
        retList.add(glAccount);
        GlAccount glAccount2 = new GlAccount();
        glAccount2.setAcctNo("321");
        retList.add(glAccount2);
        return retList;
    }

    @Override
    public String getPokus() {
        return "Pokus!!!!!!!!!!!!!!!";
    }
}

