package com.example.demosoap2.services;

import com.example.demosoap2.daos.DeadLetterQueueDao;
import com.example.demosoap2.daos.MessageByAcctNoReqDao;
import com.example.demosoap2.daos.MessageReqDao;
import com.example.demosoap2.entities.DeadLetterQueue;
import com.example.demosoap2.entities.MessageByAcctNoReq;
import com.example.demosoap2.entities.MessageReq;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
@Slf4j
public class MessageService {

    @Autowired
    private MessageReqDao messageReqDao;

    @Autowired
    private MessageByAcctNoReqDao messageByAcctNoReqDao;

    @Autowired
    private DeadLetterQueueDao deadLetterQueueDao;

    public void saveMessageReq(String messageReq) {
        final MessageReq savedReq = messageReqDao.save(new MessageReq(null, messageReq));
        log.info("Req saved " + savedReq.toString());
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public MessageByAcctNoReq createNewMessByAcctNo(MessageByAcctNoReq messageByAcctNoReq) {
        final MessageByAcctNoReq savedMessByAcctNo = messageByAcctNoReqDao.save(messageByAcctNoReq);
        log.info("mess by acctno saved " + savedMessByAcctNo.toString());
        return savedMessByAcctNo;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public MessageByAcctNoReq saveMessByAcctNo(MessageByAcctNoReq messageByAcctNoReq) {
        final MessageByAcctNoReq savedMessByAcctNo = messageByAcctNoReqDao.save(messageByAcctNoReq);
        log.info("mess by acctno saved " + savedMessByAcctNo.toString());
        return savedMessByAcctNo;
    }

    public List<MessageByAcctNoReq> findAllMessByAcctNoResponseGotNull() {
        final List<MessageByAcctNoReq> byResponseGot = messageByAcctNoReqDao.findByResponseGot(null);
        log.info("Nalezeno " + byResponseGot.size());
        return byResponseGot;
    }

    public void transferToDlq(MessageByAcctNoReq messageByAcctNoReq) {
        DeadLetterQueue deadLetterQueue = new DeadLetterQueue();
        deadLetterQueue.setCorrelationId(messageByAcctNoReq.getCorrelationId());
        deadLetterQueue.setCreationDate(messageByAcctNoReq.getCreationTime());
        deadLetterQueue.setMessageRequest(messageByAcctNoReq.getMessageRequest());
        deadLetterQueue.setServiceType(messageByAcctNoReq.getServiceType());
        deadLetterQueue.setMethodType(messageByAcctNoReq.getMethodType());
        deadLetterQueueDao.save(deadLetterQueue);
        messageByAcctNoReqDao.delete(messageByAcctNoReq);
    }
}
