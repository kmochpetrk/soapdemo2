package com.example.demosoap2;

import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QuartzConfiguration {

    @Bean
    public JobDetail jobADetails() {
        return JobBuilder.newJob(OneDaySentJob.class).withIdentity("oneDayJobSent")
                .storeDurably().build();
    }

    @Bean
    public Trigger jobATrigger(JobDetail jobADetails) {

        return TriggerBuilder.newTrigger().forJob(jobADetails)

                .withIdentity("sampleTriggerOneDay")
                .withSchedule(CronScheduleBuilder.cronSchedule("0 */1 * * * ? *"))
                .build();
    }

}
