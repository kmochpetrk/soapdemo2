package com.example.demosoap2;

import com.example.demosoap2.entities.MessageByAcctNoReq;
import com.example.demosoap2.services.MessageService;
import com.examplesoap.demosoap2.accounts.generated.BottomUpServiceImplService;
import com.examplesoap.demosoap2.accounts.generated.BottomUpServiceInterface;
import com.examplesoap.demosoap2.accounts.generated.GlAccount;
import com.sun.xml.ws.api.message.Headers;
import com.sun.xml.ws.developer.WSBindingProvider;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.batch.BatchProperties;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.soap.SOAPFaultException;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class OneDaySentJob implements Job {

    @Autowired
    private MessageService messageService;

    @Autowired
    private BottomUpServiceInterface bottomUpServiceInterface;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
       log.info("OneDayjobSent called!");
        final List<MessageByAcctNoReq> allMessByAcctNoResponseGotNull = messageService.findAllMessByAcctNoResponseGotNull();
        allMessByAcctNoResponseGotNull.forEach(a -> {
            try {
                sendAgain(a);
            } catch (Exception e) {
                log.error("nastala chyba", e);
            }
        });
    }

    public void sendAgain(MessageByAcctNoReq messageByAcctNoReq) throws Exception {
        BottomUpServiceImplService service = new BottomUpServiceImplService();
        final BottomUpServiceInterface port = service.getBottomUpServiceImplPort();
        WSBindingProvider provider = (WSBindingProvider)port;
        final Binding binding = provider.getBinding();
        List<Handler> handlerChain = new ArrayList<Handler>();
        handlerChain.addAll(binding.getHandlerChain());
        final LogMessageHandler e = new LogMessageHandler();
        e.setMessageService(messageService);
        handlerChain.add(e);
        binding.setHandlerChain(handlerChain);
        // custom header
        provider.setOutboundHeaders(Headers.create(new QName("http://services.samples", "client_id"), "22"));
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                new javax.net.ssl.HostnameVerifier(){

                    public boolean verify(String hostname,
                                          javax.net.ssl.SSLSession sslSession) {
                        if (hostname.equals("Petr-PC")) {
                            return true;
                        }
                        return false;
                    }
                });

        provider.getRequestContext().put(
                BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                "https://Petr-PC:9443/services/accounts");
        provider.getRequestContext().put("com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory", Demosoap2Application.getCustomSocketFactory());
        //List<GlAccount> allAccounts = new ArrayList<>();
        try {
            final String uuidStr = messageByAcctNoReq.getCorrelationId();
            provider.setOutboundHeaders(Headers.create(new QName("http://services.samples", "correlation_id"), uuidStr));

            JAXBContext context = JAXBContext.newInstance(GlAccount.class);
            final Unmarshaller unmarshaller = context.createUnmarshaller();

            Element node =  DocumentBuilderFactory
                    .newInstance()
                    .newDocumentBuilder()
                    .parse(new ByteArrayInputStream(messageByAcctNoReq.getMessageRequest().getBytes()))
                    .getDocumentElement();

            final GlAccount glAccountInput = unmarshaller.unmarshal(node, GlAccount.class).getValue();

//            JAXBElement<GlAccount> jaxbElementGlAccount =
//                    new JAXBElement<GlAccount>( new QName("", "glAccount"),
//                            GlAccount.class,
//                            glAccountInput);

//            m.marshal(jaxbElementGlAccount, writer);
//            MessageByAcctNoReq messageByAcctNoReq = new MessageByAcctNoReq();
//            messageByAcctNoReq.setCorrelationId(uuidStr);
//            //messageByAcctNoReq.setCreationTime(Timestamp.valueOf(LocalDateTime.now()));
//            messageByAcctNoReq.setMessageRequest(writer.toString());
//            final MessageByAcctNoReq newMessByAcctNo = messageService.createNewMessByAcctNo(messageByAcctNoReq);
            messageByAcctNoReq.setNoOfReq(messageByAcctNoReq.getNoOfReq() + 1);
            messageByAcctNoReq = messageService.saveMessByAcctNo(messageByAcctNoReq);
            final GlAccount accountByAcctnoResponse = port.getAccountByAcctno(glAccountInput);
            messageByAcctNoReq.setResponseGot(true);
            messageService.saveMessByAcctNo(messageByAcctNoReq);
            //allAccounts.add(accountByAcctnoResponse);
        } catch (SOAPFaultException sexc) {
            if (messageByAcctNoReq.getNoOfReq().equals(3)) {
                // prevest do DLQ
                messageService.transferToDlq(messageByAcctNoReq);
            }
            // nemodelovana - non wsdl exception - error handling soap
            if (sexc.getMessage().contains("Just 2 accounts")) {
                // logging
                log.error(sexc.getMessage());
                //log.error("Soap exception", sexc);
            }
        }
        //System.out.println("NUmber of accounts " + allAccounts.size());

    }
}
