package com.example.demosoap2;

import com.example.demosoap2.entities.MessageByAcctNoReq;
import com.example.demosoap2.entities.MethodType;
import com.example.demosoap2.entities.ServiceType;
import com.example.demosoap2.services.MessageService;
import com.examplesoap.demosoap2.accounts.generated.BottomUpServiceImplService;
import com.examplesoap.demosoap2.accounts.generated.BottomUpServiceInterface;
import com.examplesoap.demosoap2.accounts.generated.GlAccount;
import com.sun.xml.ws.api.message.Headers;
import com.sun.xml.ws.developer.WSBindingProvider;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.soap.SOAPFaultException;
import java.io.StringWriter;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.*;

@RestController
@Slf4j
public class AccountsRestController {

//    @Autowired
//    private BottomUpServiceImplService service;

    @Autowired
    private MessageService messageService;

    @GetMapping("/restaccountsoversoap")
    public List<GlAccount> getAccounts() throws Exception {
        BottomUpServiceImplService service = new BottomUpServiceImplService();
        final BottomUpServiceInterface port = service.getBottomUpServiceImplPort();
        WSBindingProvider provider = (WSBindingProvider)port;
        final Binding binding = provider.getBinding();
        List<Handler> handlerChain = new ArrayList<Handler>();
        handlerChain.addAll(binding.getHandlerChain());
        final LogMessageHandler e = new LogMessageHandler();
        e.setMessageService(messageService);
        handlerChain.add(e);
        binding.setHandlerChain(handlerChain);
        // custom header
        provider.setOutboundHeaders(Headers.create(new QName("http://services.samples", "client_id"), "22"));
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                new javax.net.ssl.HostnameVerifier(){

                    public boolean verify(String hostname,
                                          javax.net.ssl.SSLSession sslSession) {
                        if (hostname.equals("Petr-PC")) {
                            return true;
                        }
                        return false;
                    }
                });

        provider.getRequestContext().put(
                BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                "https://Petr-PC:9443/services/accounts");
        provider.getRequestContext().put("com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory", Demosoap2Application.getCustomSocketFactory());
        List<GlAccount> allAccounts = new ArrayList<>();
        try {
            final UUID uuid = UUID.randomUUID();
            final String uuidStr = uuid.toString();
            provider.setOutboundHeaders(Headers.create(new QName("http://services.samples", "correlation_id"), uuidStr));
            GlAccount glAccountInput = new GlAccount();
            glAccountInput.setAcctNo("333");

            StringWriter writer = new StringWriter();
            JAXBContext context = JAXBContext.newInstance(GlAccount.class);
            Marshaller m = context.createMarshaller();

            JAXBElement<GlAccount> jaxbElementGlAccount =
                    new JAXBElement<GlAccount>( new QName("http://demo.example.com/", "GlAccount"),
                            GlAccount.class,
                            glAccountInput);

            m.marshal(jaxbElementGlAccount, writer);
            MessageByAcctNoReq messageByAcctNoReq = new MessageByAcctNoReq();
            messageByAcctNoReq.setCorrelationId(uuidStr);
            messageByAcctNoReq.setCreationTime(OffsetDateTime.now());
            messageByAcctNoReq.setMethodType(MethodType.GET_ACCOUNT_BY_ACCTNO);
            messageByAcctNoReq.setServiceType(ServiceType.GL_ACCOUNTS);
            messageByAcctNoReq.setMessageRequest(writer.toString());
            messageByAcctNoReq.setNoOfReq(1);
            final MessageByAcctNoReq newMessByAcctNo = messageService.createNewMessByAcctNo(messageByAcctNoReq);


            final GlAccount accountByAcctnoResponse = port.getAccountByAcctno(glAccountInput);
            newMessByAcctNo.setResponseGot(true);
            messageService.saveMessByAcctNo(newMessByAcctNo);
            allAccounts.add(accountByAcctnoResponse);
        } catch (SOAPFaultException sexc) {
            // nemodelovana - non wsdl exception - error handling soap
            if (sexc.getMessage().contains("Just 2 accounts")) {
                // logging
                log.error(sexc.getMessage(), sexc);
                //log.error("Soap exception", sexc);
            }
        }
        System.out.println("NUmber of accounts " + allAccounts.size());
        return allAccounts;
    }

    @GetMapping("/restaccountsoverrest")
    public List<GlAccount> getAccountsRest() throws Exception {
        final String password = "Kava123";
        SSLContext ctx = SSLContext.getInstance("TLSv1.2");
        // keystore
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(Demosoap2Application.class.getClassLoader().getResourceAsStream("jks/golemkey.jks"), "Kava123".toCharArray());
        final KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(keyStore, "Kava123".toCharArray());
        X509TrustManager tm = new X509TrustManager() {

            public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
            }

            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }
        };
        ctx.init(kmf.getKeyManagers(), new TrustManager[]{tm}, null);
        //SSLSocketFactory ssf = new SSLSocketFactory(ctx,SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        CloseableHttpClient client = HttpClients.custom()
                .setSSLContext(ctx)
                .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory
                = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(client);




        RestTemplate restTemplate = new RestTemplate(requestFactory);
        ResponseEntity<List<GlAccount>> rateResponse =
                restTemplate.exchange("https://Petr-PC:9443/accountsrest",
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<GlAccount>>() {
                        });
        List<GlAccount> allAccounts = rateResponse.getBody();


        ///BottomUpServiceImplService service = new BottomUpServiceImplService();



//        final BottomUpServiceInterface port = service.getBottomUpServiceImplPort();
//        BindingProvider provider = (BindingProvider)port;
////        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
////                new javax.net.ssl.HostnameVerifier(){
////
////                    public boolean verify(String hostname,
////                                          javax.net.ssl.SSLSession sslSession) {
////                        if (hostname.equals("localhost")) {
////                            return true;
////                        }
////                        return false;
////                    }
////                });
//
//        provider.getRequestContext().put(
//                BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
//                "https://localhost:9443/services/accounts");
//        provider.getRequestContext().put("com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory", Demosoap2Application.getCustomSocketFactory());
//        final List<GlAccount> allAccounts = port.getAllAccounts();
        System.out.println("NUmber of accounts " + allAccounts.size());
        return allAccounts;
    }


    @GetMapping("/restaccountsoverrestexc")
    public List<GlAccount> getAccountsRestExc(@RequestParam Boolean isexc) throws Exception {
        final String password = "Kava123";
        SSLContext ctx = SSLContext.getInstance("TLSv1.2");
        // keystore
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(Demosoap2Application.class.getClassLoader().getResourceAsStream("jks/golemkey.jks"), "Kava123".toCharArray());
        final KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(keyStore, "Kava123".toCharArray());
        X509TrustManager tm = new X509TrustManager() {

            public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
            }

            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }
        };
        ctx.init(kmf.getKeyManagers(), new TrustManager[]{tm}, null);
        //SSLSocketFactory ssf = new SSLSocketFactory(ctx,SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        CloseableHttpClient client = HttpClients.custom()
                .setSSLContext(ctx)
                .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory
                = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(client);




        RestTemplate restTemplate = new RestTemplate(requestFactory);
        ResponseEntity<List<GlAccount>> rateResponse = null;
        List<GlAccount> allAccounts = Collections.emptyList();
        Map<String, String> params = new HashMap<>();
        params.put("isexc", String.valueOf(isexc));
        try {
            rateResponse =
                    restTemplate.exchange("https://Petr-PC:9443/accountsrestwithexc?isexc={isexc}",
                            HttpMethod.GET, null, new ParameterizedTypeReference<List<GlAccount>>() {
                            }, params);
            allAccounts = rateResponse.getBody();
        } catch (HttpClientErrorException exc) {
            log.info("Nastala vyjimka", exc);
            //throw new IllegalStateException(exc);
            // error handling rest
            if (exc.getStatusCode().equals(HttpStatus.NOT_FOUND) && exc.getMessage().contains("No account found")) {
                log.info("No account found");
            }
        }



        ///BottomUpServiceImplService service = new BottomUpServiceImplService();



//        final BottomUpServiceInterface port = service.getBottomUpServiceImplPort();
//        BindingProvider provider = (BindingProvider)port;
////        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
////                new javax.net.ssl.HostnameVerifier(){
////
////                    public boolean verify(String hostname,
////                                          javax.net.ssl.SSLSession sslSession) {
////                        if (hostname.equals("localhost")) {
////                            return true;
////                        }
////                        return false;
////                    }
////                });
//
//        provider.getRequestContext().put(
//                BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
//                "https://localhost:9443/services/accounts");
//        provider.getRequestContext().put("com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory", Demosoap2Application.getCustomSocketFactory());
//        final List<GlAccount> allAccounts = port.getAllAccounts();
        System.out.println("NUmber of accounts " + allAccounts.size());
        return allAccounts;
    }
}
