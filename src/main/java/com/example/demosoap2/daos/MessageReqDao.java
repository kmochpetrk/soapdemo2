package com.example.demosoap2.daos;

import com.example.demosoap2.entities.MessageReq;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageReqDao extends JpaRepository<MessageReq, Long> {
}
