package com.example.demosoap2.daos;

import com.example.demosoap2.entities.MessageByAcctNoReq;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageByAcctNoReqDao extends JpaRepository<MessageByAcctNoReq, Long> {

    public List<MessageByAcctNoReq> findByResponseGot(Boolean responseGot);
}
