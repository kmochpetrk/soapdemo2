package com.example.demosoap2.daos;

import com.example.demosoap2.entities.DeadLetterQueue;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeadLetterQueueDao extends JpaRepository<DeadLetterQueue, Long> {
}
