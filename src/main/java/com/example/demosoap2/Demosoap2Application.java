package com.example.demosoap2;

import com.example.demosoap2.entities.MessageByAcctNoReq;
import com.example.demosoap2.services.MessageService;
import com.examplesoap.demosoap2.accounts.generated.BottomUpServiceImplService;
import com.examplesoap.demosoap2.accounts.generated.BottomUpServiceInterface;
import com.examplesoap.demosoap2.accounts.generated.GlAccount;
import com.sun.xml.ws.api.message.Headers;
import com.sun.xml.ws.developer.WSBindingProvider;
import com.sun.xml.ws.transport.http.servlet.SpringBinding;
import com.sun.xml.ws.transport.http.servlet.WSSpringServlet;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.jvnet.jax_ws_commons.spring.SpringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Element;

import javax.annotation.PostConstruct;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.servlet.Servlet;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.soap.SOAPFaultException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SpringBootApplication
//@EnableScheduling
@ComponentScan(basePackages = {"com.example.demosoap2", "com.examplesoap.demosoap2"})
@Slf4j
public class Demosoap2Application {

//    @Autowired
//    private BottomUpServiceImplService service;

    @Autowired
    private MessageService messageService;

    @Autowired
    private BottomUpServiceInterface bottomUpServiceInterface;

    public static void main(String[] args) {
        SpringApplication.run(Demosoap2Application.class, args);
    }

    //@Scheduled(initialDelay = 5000, fixedRate = 1000*60*60*24 )
    public void checkNotSent() throws Exception {
        final List<MessageByAcctNoReq> allMessByAcctNoResponseGotNull = messageService.findAllMessByAcctNoResponseGotNull();
        allMessByAcctNoResponseGotNull.forEach(a -> {
            try {
                sendAgain(a);
            } catch (Exception e) {
                log.error("nastala chyba", e);
            }
        });
    }


    public void sendAgain(MessageByAcctNoReq messageByAcctNoReq) throws Exception {
        BottomUpServiceImplService service = new BottomUpServiceImplService();
        final BottomUpServiceInterface port = service.getBottomUpServiceImplPort();
        WSBindingProvider provider = (WSBindingProvider)port;
        final Binding binding = provider.getBinding();
        List<Handler> handlerChain = new ArrayList<Handler>();
        handlerChain.addAll(binding.getHandlerChain());
        final LogMessageHandler e = new LogMessageHandler();
        e.setMessageService(messageService);
        handlerChain.add(e);
        binding.setHandlerChain(handlerChain);
        // custom header
        provider.setOutboundHeaders(Headers.create(new QName("http://services.samples", "client_id"), "22"));
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                new javax.net.ssl.HostnameVerifier(){

                    public boolean verify(String hostname,
                                          javax.net.ssl.SSLSession sslSession) {
                        if (hostname.equals("Petr-PC")) {
                            return true;
                        }
                        return false;
                    }
                });

        provider.getRequestContext().put(
                BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                "https://Petr-PC:9443/services/accounts");
        provider.getRequestContext().put("com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory", Demosoap2Application.getCustomSocketFactory());
        //List<GlAccount> allAccounts = new ArrayList<>();
        try {
            final String uuidStr = messageByAcctNoReq.getCorrelationId();
            provider.setOutboundHeaders(Headers.create(new QName("http://services.samples", "correlation_id"), uuidStr));

            JAXBContext context = JAXBContext.newInstance(GlAccount.class);
            final Unmarshaller unmarshaller = context.createUnmarshaller();

            Element node =  DocumentBuilderFactory
                    .newInstance()
                    .newDocumentBuilder()
                    .parse(new ByteArrayInputStream(messageByAcctNoReq.getMessageRequest().getBytes()))
                    .getDocumentElement();

            final GlAccount glAccountInput = unmarshaller.unmarshal(node, GlAccount.class).getValue();

//            JAXBElement<GlAccount> jaxbElementGlAccount =
//                    new JAXBElement<GlAccount>( new QName("", "glAccount"),
//                            GlAccount.class,
//                            glAccountInput);

//            m.marshal(jaxbElementGlAccount, writer);
//            MessageByAcctNoReq messageByAcctNoReq = new MessageByAcctNoReq();
//            messageByAcctNoReq.setCorrelationId(uuidStr);
//            //messageByAcctNoReq.setCreationTime(Timestamp.valueOf(LocalDateTime.now()));
//            messageByAcctNoReq.setMessageRequest(writer.toString());
//            final MessageByAcctNoReq newMessByAcctNo = messageService.createNewMessByAcctNo(messageByAcctNoReq);
            messageByAcctNoReq.setNoOfReq(messageByAcctNoReq.getNoOfReq() + 1);
            messageByAcctNoReq = messageService.saveMessByAcctNo(messageByAcctNoReq);
            final GlAccount accountByAcctnoResponse = port.getAccountByAcctno(glAccountInput);
            messageByAcctNoReq.setResponseGot(true);
            messageService.saveMessByAcctNo(messageByAcctNoReq);
            //allAccounts.add(accountByAcctnoResponse);
        } catch (SOAPFaultException sexc) {
            if (messageByAcctNoReq.getNoOfReq().equals(3)) {
                // prevest do DLQ
                messageService.transferToDlq(messageByAcctNoReq);
            }
            // nemodelovana - non wsdl exception - error handling soap
            if (sexc.getMessage().contains("Just 2 accounts")) {
                // logging
                log.error(sexc.getMessage());
                //log.error("Soap exception", sexc);
            }
        }
        //System.out.println("NUmber of accounts " + allAccounts.size());

    }

    public static SSLSocketFactory getCustomSocketFactory() throws Exception {

        // Create and load the truststore
        KeyStore trustStore = KeyStore.getInstance("JKS");
        trustStore.load(Demosoap2Application.class.getClassLoader().getResourceAsStream("jks/golem222.jks"), "Kava123".toCharArray());

        // keystore
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(Demosoap2Application.class.getClassLoader().getResourceAsStream("jks/golemkey.jks"), "Kava123".toCharArray());
        final KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(keyStore, "Kava123".toCharArray());

        // Create and initialize the truststore manager
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
        tmf.init(trustStore);

        // Create and initialize the SSL context
        SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
        sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());
        return sslContext.getSocketFactory();
    }

    @Bean
    public Servlet servlet() {
        return new WSSpringServlet();
    }

    @Bean
    public ServletRegistrationBean<Servlet> servletRegistrationBean(){
        return new ServletRegistrationBean<Servlet>(servlet(), "/services/*");
    }

    @Bean()
    public SpringBinding springBinding() throws Exception {
        SpringService service=new SpringService();
        service.setBean(bottomUpServiceInterface);
        SpringBinding binding=new SpringBinding();
        binding.setService(service.getObject());
        binding.setUrl("/services/accounts");
        return binding;
    }

}
