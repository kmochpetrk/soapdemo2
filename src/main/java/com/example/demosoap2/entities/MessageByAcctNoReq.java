package com.example.demosoap2.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table(name = "account_by_acctno_req")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageByAcctNoReq {

    @Id
    @SequenceGenerator(name="account_by_acctno_req_seq",
            sequenceName="account_by_acctno_req_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="account_by_acctno_req_seq")
    @Column(name = "req_id")
    private Long id;

    //@Lob
    @Column(name = "content_req")
    private String messageRequest;

    @Column(name = "correlation_id")
    private String correlationId;

    @Column(name = "creation_time", columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime creationTime;

    @Column(name = "response")
    private Boolean responseGot;

    @Column(name = "no_of_reqs")
    private Integer noOfReq;

    @Enumerated(EnumType.STRING)
    @Column(name = "method_type")
    private MethodType methodType;

    @Enumerated(EnumType.STRING)
    @Column(name = "service_type")
    private ServiceType serviceType;
}
