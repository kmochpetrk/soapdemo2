package com.example.demosoap2.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@Entity
@Table(name = "dead_letter_queue")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeadLetterQueue {

    @Id
    @SequenceGenerator(name="dead_letter_queue_seq",
            sequenceName="dead_letter_queue_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="dead_letter_queue_seq")
    @Column(name = "req_id")
    private Long id;

    //@Lob
    @Column(name = "content_req")
    private String messageRequest;

    @Column(name = "correlation_id")
    private String correlationId;

    @Column(name = "creation_date", columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime creationDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "method_type")
    private MethodType methodType;

    @Enumerated(EnumType.STRING)
    @Column(name = "service_type")
    private ServiceType serviceType;
}
