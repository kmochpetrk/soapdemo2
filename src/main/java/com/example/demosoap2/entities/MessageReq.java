package com.example.demosoap2.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "message_req")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageReq {

    @Id
    @SequenceGenerator(name="webuser_idwebuser_seq",
            sequenceName="webuser_idwebuser_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="webuser_idwebuser_seq")
    @Column(name = "req_id")
    private Long id;

    //@Lob
    @Column(name = "content_req")
    private String messageRequest;

}
